#!/usr/bin/env node

var Cabal = require('cabal-core')
var swarm = require('cabal-core/swarm.js')
var minimist = require('minimist')
var os = require('os')
var fs = require('fs')
var path = require('path')
var yaml = require('js-yaml')
var mkdirp = require('mkdirp')
var frontend = require('./neat-screen.js')
var crypto = require('hypercore-crypto')
var chalk = require('chalk')
var ram = require("random-access-memory")
var serialport = require('serialport');

var args = minimist(process.argv.slice(2))

var homedir = os.homedir()
var rootdir = args.dir || (homedir + `/.cabal/v${Cabal.databaseVersion}`)
var rootconfig = `${rootdir}/config.yml`
var archivesdir = `${rootdir}/archives/`


var express = require('express')

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const formatData = require('./helpers/formatData');

const bodyParser = require('body-parser')

const adapter = new FileSync('db.json')
const db = low(adapter)
const lodashId = require('lodash-id')

var config;
try{
  config = require('./config.json')
}catch(err){
  console.warn('config.json not set up!')
}

// serialport
// list serial ports:
serialport.list(function (err, ports) {
  ports.forEach(function(port) {
    console.log(port.comName);
  });
});

// cabal setup
var cabal = Cabal(archivesdir + config.key, config.key, {maxFeeds: maxFeeds})
swarm(cabal,() => console.log(`cabal swarming key ${config.key}!`))
var channel = config.channel

// server + database setup
var app = express()
const port = args.port || 4000;


db.defaults({ history: [] })
  .write()


app.use(bodyParser.json())

app.post('/', function (req, res) {
  //console.log(req.body);
  const formattedData = formatData(req.body);
  const entry = db.get('history')
    .push(formattedData)
    .write()
  
  
  var used = process.memoryUsage().heapUsed / 1024 / 1024;
  console.log(`${used}`);
  
  res.sendStatus(200)
  
  publishSingleMessage({
        key: config.key,
        channel: channel,
        message: JSON.stringify(req.body),
        messageType: 'chat/text',
        timeout: config.timeout
      })
      
});

app.listen(port, () => console.log(`Geoduck listening on port ${port}!`))

if (args.version || args.v) {
  console.log(JSON.parse(fs.readFileSync(path.join(__dirname, 'package.json'), 'utf8')).version)
  process.exit(0)
}

if (args.help || args.h) {
  process.stderr.write(usage)
  process.exit(1)
}

var config
var cabalKeys = []
var configFilePath = findConfigPath()
var maxFeeds = 1000

// make sure the .cabal/v<databaseVersion> folder exists
mkdirp.sync(rootdir)

// create a default config in rootdir if it doesn't exist
if (!fs.existsSync(rootconfig)) {
  saveConfig(rootconfig, { cabals: [], aliases: {} })
}

// Attempt to load local or homedir config file
try {
  if (configFilePath) {
    config = yaml.safeLoad(fs.readFileSync(configFilePath, 'utf8'))
    if (!config.cabals) { config.cabals = [] }
    if (!config.aliases) { config.aliases = {} }
    cabalKeys = config.cabals
  }
} catch (e) {
  logError(e)
  process.exit(1)
}


function logError (msg) {
  console.error(`${chalk.red('cabal:')} ${msg}`)
}

function findConfigPath () {
  var currentDirConfigFilename = '.cabal.yml'
  if (args.config && fs.existsSync(args.config)) {
    return args.config
  } else if (fs.existsSync(currentDirConfigFilename)) {
    return currentDirConfigFilename
  }
  return rootconfig
}

function saveConfig (path, config) {
  // make sure config is well-formatted (contains all config options)
  if (!config.cabals) { config.cabals = [] }
  if (!config.aliases) { config.aliases = {} }
  let data = yaml.safeDump(config, {
    sortKeys: true
  })
  fs.writeFileSync(path, data, 'utf8')
}

function publishSingleMessage ({key, channel, message, messageType, timeout}) {
  //console.log(`Publishing message to channel - ${channel || 'default'}: ${message}`)
  //var cabal = Cabal(archivesdir + key, key, {maxFeeds: maxFeeds})
  //var swarm = require('cabal-core/swarm.js')
  cabal.db.ready(() => {
    cabal.publish({
      type: messageType || 'chat/text',
      content: {
        channel: channel || 'default',
        text: message
      }
    })
  })
}

